import React, {Component} from 'react'
import "./App.css"

class QuizQuestionButton extends Component {
    handleClick() {
        this.props.clickHandler(this.props.button_text)
    }
    render() {
        const {button_text}=this.props
        return (
            <li className='button_list'>
                <button className='option' onClick={this.handleClick.bind(this)}>{button_text}</button>
            </li>
        )
    }
}

export default QuizQuestionButton