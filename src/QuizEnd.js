import React, {Component} from 'react'
import "./App.css"

class QuizEnd extends Component {

    handleResetClick() {
        this.props.resetClickHandler()
    }
    render() {
        return(
            <div className='quiz_end'>
                <p className='thanks'>Thanks for playing!</p>
                <a className='anchor' href='' onClick={this.handleResetClick.bind(this)}>Reset Quiz</a>
            </div>
        )
    }
}

export default QuizEnd