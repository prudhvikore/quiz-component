import React,{Component} from 'react'
import QuizQuestionButton from './QuizQuestionButton'
import "./App.css"


class QuizQuestion extends Component {

    constructor(props) {
        super(props);
        this.state={incorrectAnswer:false}
    }

    handleClick(buttonText) {
        if (buttonText===this.props.quiz_question.answer) {
            this.props.showNextQuestionHandler()
            this.setState({incorrectAnswer:false})
        }
        else {
            this.setState({incorrectAnswer:true})
        }
    }

    render() {
        const {quiz_question} = this.props
        const {incorrectAnswer}=this.state
        return (
      <main>
        <section>
          <p className='QuizQuestion'>{quiz_question.instruction_text}</p>
        </section>
        <section className="buttons">
          <ul className='option_list'>
            {this.props.quiz_question.answer_options.map((answer_option,index)=><QuizQuestionButton button_text={answer_option} key={index} clickHandler={this.handleClick.bind(this)}/>)}
          </ul>
        </section>
        <div className='error_text'>
        {incorrectAnswer?<p className='error'>Sorry, that's not right</p>:null}
        </div>
        
      </main>
        )
    }
}

export default QuizQuestion